package com.acooly.module.openapi.client.provider.wft.message;

import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wft.domain.WftApiMsgInfo;
import com.acooly.module.openapi.client.provider.wft.domain.WftRequest;
import com.acooly.module.openapi.client.provider.wft.enums.WftServiceEnum;
import com.acooly.module.openapi.client.provider.wft.support.WftAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2017/11/28 21:10
 */
@Getter
@Setter
@WftApiMsgInfo(service = WftServiceEnum.PAY_WEIXIN_JSPAY, type = ApiMessageType.Request)
public class WftPayWeixinJspayRequest extends WftRequest {

    /**
     * 原生JS
     */
    @WftAlias(value = "is_raw")
    @Size(max = 1)
    private String isRaw = "1";

    /**
     * 是否小程序支付,值为1，表示小程序支付；不传或值不为1，表示公众账号内支付
     */
    @WftAlias(value = "is_minipg")
    @Size(max = 1)
    private String isMinipg;

    /**
     * 订单号
     */
    @WftAlias(value = "out_trade_no")
    @NotBlank(message = "订单号不能为空")
    @Size(max = 32)
    private String outTradeNo;

    /**
     * 设备号
     */
    @WftAlias(value = "device_info")
    @Size(max = 32)
    private String deviceInfo;

    /**
     * 商品描述
     */
    @WftAlias(value = "body")
    @Size(max = 127)
    @NotBlank(message = "商品描述不能为空")
    private String body;

    /**
     * 用户openid,微信用户关注商家公众号的openid（注：使用测试号时此参数置空，即不要传这个参数，使用正式商户号时才传入，参数名是sub_openid，具体请看文档最后注意事项第7点）
     */
    @WftAlias(value = "sub_openid")
    @Size(max = 128)
    private String subOpenid;

    /**
     * 公众账号或小程序ID,当发起公众号支付时，值是微信公众平台基本配置中的AppID(应用ID)；当发起小程序支付时，值是对应小程序的AppID
     */
    @WftAlias(value = "sub_appid")
    @Size(max = 127)
    private String subAppid;

    /**
     * 附加信息
     */
    @WftAlias(value = "attach")
    private String attach;

    /**
     * 总金额，以分为单位，不允许包含任何字、符号
     */
    @WftAlias(value = "total_fee")
    @NotBlank(message = "金额不能为空")
    private String amount;

    /**
     * 终端IP，订单生成的机器 IP
     */
    @WftAlias(value = "mch_create_ip")
    @NotBlank(message = "终端IP不能为空")
    @Size(max = 16)
    private String mchCreateIp;

    /**
     * 接收平台通知的URL，需给绝对路径，255字符内格式如:http://wap.tenpay.com/tenpay.asp，确保平台能通过互联网访问该地址
     */
    @WftAlias(value = "notify_url")
    @NotBlank(message = "异步通知地址不能为空")
    @Size(max = 255)
    private String notifyUrl;

    /**
     * 订单生成时间 订单生成时间，格式为yyyymmddhhmmss，如2009年12月25日9点10分10秒表示为20091225091010。时区为GMT+8
     * beijing。该时间取自商户服务器。注：订单生成时间与超时时间需要同时传入才会生效。
     */
    @WftAlias(value = "time_start")
    @Size(max = 14)
    private String timeStart;

    /**
     * 订单超时时间 订单失效时间，格式为yyyymmddhhmmss，如2009年12月27日9点10分10秒表示为20091227091010。时区为GMT+8
     * beijing。该时间取自商户服务器。注：订单生成时间与超时时间需要同时传入才会生效。
     */
    @WftAlias(value = "time_expire")
    @Size(max = 14)
    private String timeExpire;

    /**
     * 商品标记
     */
    @WftAlias(value = "goods_tag")
    @Size(max = 32)
    private String goodsTag;

    /**
     * 随机字符串
     */
    @WftAlias(value = "nonce_str")
    @Size(max = 32)
    private String nonceStr = Ids.oid();

    /**
     * 限定用户使用时能否使用信用卡，值为1，禁用信用卡；值为0或者不传此参数则不禁用
     */
    @WftAlias(value = "limit_credit_pay")
    @Size(max = 32)
    private String limitCreditPay = "0";
}
