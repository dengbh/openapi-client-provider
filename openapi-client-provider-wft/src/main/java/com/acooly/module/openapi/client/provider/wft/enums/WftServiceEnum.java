/*
 * ouwen@yiji.com Inc.
 * Copyright (c) 2017 All Rights Reserved.
 * create by ouwen
 * date:2017-03-30
 *
 */
package com.acooly.module.openapi.client.provider.wft.enums;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * inst_channel_api DebitCreditType 枚举定义
 *
 * @author zhike Date: 2017-03-30 15:12:06
 */
public enum WftServiceEnum implements Messageable {
  UNIFIED_TRADE_MICROPAY("unified.trade.micropay", "unifiedTradeMicropay", "小额支付（微信、支付宝被扫）"),
  TRADE_ORDER_QUERY("unified.trade.query", "unifiedTradeQuery", "订单查询"),
  UNIFIED_MICROPAY_REVERSE("unified.micropay.reverse", "unifiedMicropayReverse", "撤销订单"),
  UNIFIED_TRADE_REFUND("unified.trade.refund", "unifiedTradeRefund", "申请退款"),
  UNIFIED_TRADE_REFUNDQUERY("unified.trade.refundquery", "unifiedTradeRefundquery", "退款状态查询"),
  PAY_WEIXIN_NATIVE("pay.weixin.native", "payWeixinNative", "微信主扫支付"),
  UNIFIED_TRADE_CLOSE("unified.trade.close", "unifiedTradeClose", "订单关闭"),
  PAY_ALIPAY_NATIVE("pay.alipay.native", "payAlipayNative", "支付宝扫码支付"),
  PAY_WEIXIN_JSPAY("pay.weixin.jspay", "payWeixinJspay", "微信公众号支付"),
  PAY_WEIXIN_RAW_APP("pay.weixin.raw.app", "payWeixinRawApp", "微信APP支付"),
  BILL_DOWNLOAD("pay.bill.merchant", "payBillMerchant", "下载对账文件"),
  ;

  private final String code;
  private final String key;
  private final String message;

  private WftServiceEnum(String code, String key, String message) {
    this.code = code;
    this.message = message;
    this.key = key;
  }

  public String getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }

  public String code() {
    return code;
  }

  public String message() {
    return message;
  }

  public String getKey() {
    return key;
  }

  public static Map<String, String> mapping() {
    Map<String, String> map = new LinkedHashMap<String, String>();
    for (WftServiceEnum type : values()) {
      map.put(type.getCode(), type.getMessage());
    }
    return map;
  }

  /**
   * 通过枚举值码查找枚举值。
   *
   * @param code 查找枚举值的枚举值码。
   * @return 枚举值码对应的枚举值。
   * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
   */
  public static WftServiceEnum find(String code) {
    for (WftServiceEnum status : values()) {
      if (status.getCode().equals(code)) {
        return status;
      }
    }
    return null;
  }

  /**
   * 通过枚举值码查找枚举值。
   *
   * @param key 查找枚举值的枚举值码。
   * @return 枚举值码对应的枚举值。
   * @throws IllegalArgumentException 如果 key 没有对应的 Status 。
   */
  public static WftServiceEnum findByKey(String key) {
    for (WftServiceEnum status : values()) {
      if (status.getKey().equals(key)) {
        return status;
      }
    }
    return null;
  }

  /**
   * 获取全部枚举值。
   *
   * @return 全部枚举值。
   */
  public static List<WftServiceEnum> getAll() {
    List<WftServiceEnum> list = new ArrayList<WftServiceEnum>();
    for (WftServiceEnum status : values()) {
      list.add(status);
    }
    return list;
  }

  /**
   * 获取全部枚举值码。
   *
   * @return 全部枚举值码。
   */
  public static List<String> getAllCode() {
    List<String> list = new ArrayList<String>();
    for (WftServiceEnum status : values()) {
      list.add(status.code());
    }
    return list;
  }
}
