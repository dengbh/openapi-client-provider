/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-22 04:14:52 创建
 */package com.acooly.module.openapi.client.provider.fudian.message.fund;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianResponse;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author zhangpu 2018-02-22 04:14:52
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.LOAN_CALLBACK ,type = ApiMessageType.Response)
public class LoanCallbackNotify extends FudianResponse {

    /**
     * 本金
     * 金额：元
     */
    @NotEmpty
    @Length(max=20)
    private String capital;

    /**
     * 利息
     * 金额：元
     */
    @NotEmpty
    @Length(max=20)
    private String interest;

    /**
     * 利息管理费
     * 金额：元
     */
    @NotEmpty
    @Length(max=20)
    private String interestFee;

    /**
     * 加息利息
     * 金额：元
     */
    @NotEmpty
    @Length(max=20)
    private String rateInterest;

    /**
     * 存管账户编号
     * 原有标的，投资人的存管账户账户号
     */
    @NotEmpty
    @Length(max=50)
    private String investAccountNo;

    /**
     * 原有投资订单日期
     * 原有标的，投资时使用的订单日期
     */
    @NotEmpty
    @Length(min = 8,max=8)
    private String investOrderDate;

    /**
     * 原有投资订单号
     * 原有标的，投资时使用的订单号
     */
    @NotEmpty
    @Length(max=50)
    private String investOrderNo;

    /**
     * 存管账户用户名
     * 原有标的，投资人的存管账户用户名
     */
    @NotEmpty
    @Length(max=32)
    private String investUserName;

    /**
     * 返回码
     * 0000标识成功，其他详见附录
     */
    @NotEmpty
    @Length(min = 4,max=4)
    private String retCode;

    /**
     * 返回信息
     * 返回码对应的中文或英文解释信息
     */
    @NotEmpty
    @Length(max=128)
    private String retMsg;
}