/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-22 04:14:52 创建
 */package com.acooly.module.openapi.client.provider.fudian.message.fund;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianRequest;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author zhangpu 2018-02-22 04:14:52
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.LOAN_CANCEL ,type = ApiMessageType.Request)
public class LoanCancelRequest extends FudianRequest {

    /**
     * 发标的订单日期
     * 发标的时候的订单流水日期
     */
    @NotEmpty
    @Length(min = 8,max=8)
    private String loanOrderDate;

    /**
     * 发标的订单流水号
     * 发标的时候的订单流水号
     */
    @NotEmpty
    @Length(min = 20,max=20)
    private String loanOrderNo;

    /**
     * 标的号
     * 标的号，由存管系统生成并确保唯一性.
     */
    @NotEmpty
    @Length(max=32)
    private String loanTxNo;

}