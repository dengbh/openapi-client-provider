/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-22 01:57:38 创建
 */
package com.acooly.module.openapi.client.provider.fudian.message.other;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianNotify;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author zhangpu 2018-02-22 01:57:38
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.MIGRATION_DATA, type = ApiMessageType.Notify)
public class MigrationDataNotify extends FudianNotify {


    /**
     * 文件名称
     * ${merchant_no}
     */
    @NotEmpty
    @Length(max = 256)
    private String fileName;
}