/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-10 03:06:23 创建
 */package com.acooly.module.openapi.client.provider.fudian.message.member;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianNotify;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author zhangpu 2018-02-10 03:06:23
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.CORP_MODIFY ,type = ApiMessageType.Notify)
public class CorpModifyNotify extends FudianNotify {

    /**
     * 法人代表证件号
     * 证件类型为身份证时，仅支持18位长度
     */
    @NotEmpty
    @Length(min = 18,max=18)
    private String artificialIdentityCode;

    /**
     * 法人代表真实姓名
     * 企业法人的真实姓名
     */
    @NotEmpty
    @Length(max=60)
    private String artificialRealName;

    /**
     * 企业名称
     * 企业信息变更时类型为企业名称时填写的新企业名称
     */
    @NotEmpty
    @Length(max=256)
    private String corpName;

    /**
     * 商户号
     * 用于校验主体参数和业务参数一致性，保证参数的安全传输
     */
    @NotEmpty
    @Length(max=8)
    private String merchantNo;

    /**
     * 联系人手机号
     * 联系人手机号
     */
    @NotEmpty
    @Length(min = 11,max=11)
    private String mobilePhone;

    /**
     * 变更类型
     * 变更类型：1 -企业名称, 2 -法人代表， 3- 联系人手机
     */
    @NotEmpty
    @Length(min = 1,max=1)
    private String modifyType;

    /**
     * 原企业名称
     * 企业开户时填写的企业名称
     */
    @NotEmpty
    @Length(max=256)
    private String oldCorpName;

    /**
     * 用户名
     * 企业用户在本系统的唯一标识，由本系统生成
     */
    @NotEmpty
    @Length(max=32)
    private String userName;
}