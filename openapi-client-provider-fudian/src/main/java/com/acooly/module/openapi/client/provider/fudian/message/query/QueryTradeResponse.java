/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-22 04:32:16 创建
 */package com.acooly.module.openapi.client.provider.fudian.message.query;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianResponse;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author zhangpu 2018-02-22 04:32:16
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.QUERY_TRADE ,type = ApiMessageType.Response)
public class QueryTradeResponse extends FudianResponse {

    /**
     * 商户号
     * 用于校验主体参数和业务参数一致性，保证参数的安全传输
     */
    @NotEmpty
    @Length(max=8)
    private String merchantNo;

    /**
     * 查询订单日期
     * 查询订单的日期，辅助标识
     */
    @NotEmpty
    @Length(min = 20,max=20)
    private String queryOrderNo;

    /**
     * 查询订单流水号
     * 查询订单的唯一标识
     */
    @NotEmpty
    @Length(min = 8,max=8)
    private String queryOrderDate;

    /**
     * 查询状态
     * 01：1成功、2失败、4银行处理中，02：0申请提现、1提现成功、2提现失败、3提现失败银行退单、4提现异常，"03：0:可投资
     */
    @NotEmpty
    @Length(max=2)
    private String queryState;

    /**
     * 交易类型
     * 取值范围：01充值，02提现，03投标，04借款人还款，05投资人回款，06债权认购，07满标放款
     */
    @NotEmpty
    @Length(min = 2,max=2)
    private String queryType;
}