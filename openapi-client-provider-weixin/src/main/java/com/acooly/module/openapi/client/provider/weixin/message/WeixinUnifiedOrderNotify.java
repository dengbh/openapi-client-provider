package com.acooly.module.openapi.client.provider.weixin.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.weixin.domain.WeixinApiMsgInfo;
import com.acooly.module.openapi.client.provider.weixin.domain.WeixinNotify;
import com.acooly.module.openapi.client.provider.weixin.enums.WeixinServiceEnum;
import com.acooly.module.openapi.client.provider.weixin.support.WeixinAlias;

import lombok.Getter;
import lombok.Setter;

@WeixinApiMsgInfo(service=WeixinServiceEnum.PAY_WEIXIN_UNIFIED_ORDER,type=ApiMessageType.Notify)
@Setter
@Getter
public class WeixinUnifiedOrderNotify extends WeixinNotify{

	/** 用户是否关注公众账号，Y-关注，N-未关注 */
	@WeixinAlias(value = "is_subscribe")
	private String isSubscribe;
	/** 交易类型 JSAPI、NATIVE、APP */
	@WeixinAlias(value = "trade_type")
	private String tradeType;
	/** 订单总金额，单位为分 */
	@WeixinAlias(value = "total_fee")
	private String totalFee;
	/** 支付完成时间，格式为yyyyMMddHHmmss */
	@WeixinAlias(value = "time_end")
	private String timeEnd;
}
