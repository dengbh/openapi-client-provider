/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年4月5日
 *
 */
package com.acooly.module.openapi.client.provider.weixin.support;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * @author zhangpu
 */
public class WeixinRespCodes {

	static Map<String, String> data = Maps.newHashMap();
	
	static {
	}

	public static String getMessage(String respCode) {
		return data.get(respCode) == null ? "未知错误" : data.get(respCode);
	}

}
