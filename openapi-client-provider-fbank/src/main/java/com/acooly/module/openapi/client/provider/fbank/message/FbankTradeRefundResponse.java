package com.acooly.module.openapi.client.provider.fbank.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fbank.domain.FbankApiMsgInfo;
import com.acooly.module.openapi.client.provider.fbank.domain.FbankResponse;
import com.acooly.module.openapi.client.provider.fbank.enums.FbankServiceEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike@qq.com
 * @date 2018-09-07 10:22
 */
@Getter
@Setter
@FbankApiMsgInfo(service = FbankServiceEnum.TRADE_REFUND, type = ApiMessageType.Response)
public class FbankTradeRefundResponse extends FbankResponse {

    /**
     * 退款商户订单号
     * 商户自己平台的订单号
     */
    @NotBlank
    @Size(max = 64)
    private String mchntOrderNo;

    /**
     * 平台退款号
     * 由支付平台生成
     */
    @NotBlank
    @Size(max = 50)
    private String refOrderNo;

    /**
     * 退款金额
     * 退款金额（分）
     */
    @NotBlank
    @Size(max = 12)
    private String refundFee;
}
