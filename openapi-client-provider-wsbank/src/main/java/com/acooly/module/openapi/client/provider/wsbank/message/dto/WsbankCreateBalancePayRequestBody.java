package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;


@Getter
@Setter
@XStreamAlias("body")
public class WsbankCreateBalancePayRequestBody implements Serializable {

	private static final long serialVersionUID = -147467630667068445L;

	/**
     * 合作方机构号（网商银行分配）
     */
    @Size(max = 32)
    @XStreamAlias("IsvOrgId")
    @NotBlank
    private String isvOrgId;

    /**
     * 付款方商户号
     */
    @Size(max = 32)
    @XStreamAlias("PayerMerchantId")
    @NotBlank
    private String payerMerchantId;

    /**
     * 收款方Id，如果是商户,填入merchantId,如果是平台,则填入IsvOrgId
     */
    @Size(max = 32)
    @XStreamAlias("PayeeId")
    @NotBlank
    private String payeeId;

    /**
     * 收款方类型，MERCHANT： 外部商户，PLATFORM：
     * 平台
     */
    @Size(max = 16)
    @XStreamAlias("PayeeType")
    @NotBlank
    private String PayeeType;

    /**
     * 外部订单请求流水号
     */
    @Size(max = 64)
    @XStreamAlias("OutTradeNo")
    @NotBlank
    private String outTradeNo;

    /**
     *订单金额(金额为分)
     */
    @XStreamAlias("TotalAmount")
    @NotBlank
    private String totalAmount;

    /**
     * 币种，默认CNY
     */
    @Size(max = 3)
    @XStreamAlias("Currency")
    @NotBlank
    private String currency = "CNY";

    /**
     * 商品描述。该信息将透传至第三方支付公司系统，并在客户端明细中展示。格式要求：店名-销售商品类目
     */
    @Size(max = 256)
    @XStreamAlias("Body")
    @NotBlank
    private String body;

    /**
     *商品标记。微信支付代金券或立减优惠功能的参数
     */
    @Size(max = 64)
    @XStreamAlias("GoodsTag")
    private String goodsTag;

    /**
     * 商品详情列表。JSON格式，会透传至第三方支付
     */
    @Size(max = 256)
    @XStreamAlias("GoodsDetail")
    private String goodsDetail;

    /**
     * 备注
     */
    @Size(max = 256)
    @XStreamAlias("Memo")
    private String memo;
}
