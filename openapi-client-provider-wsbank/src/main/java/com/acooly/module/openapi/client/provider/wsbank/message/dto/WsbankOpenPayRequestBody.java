package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.core.utils.Dates;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;


@Getter
@Setter
@XStreamAlias("body")
public class WsbankOpenPayRequestBody implements Serializable {

	private static final long serialVersionUID = -1667511578301725773L;

	/**
     * 合作方机构号（网商银行分配）。
     */
    @Size(max = 32)
    @XStreamAlias("IsvOrgId")
    @NotBlank
    private String isvOrgId;

    /**
     * 商户号。网商为商户分配的商户号，通过商户入驻结果查询接口获取
     */
    @Size(max = 32)
    @XStreamAlias("MerchantId")
    @NotBlank
    private String merchantId;

    /**
     * 外部交易号。合作方系统生成的外部交易号，同一交易号被视为同一笔交易
     */
    @Size(max = 32)
    @XStreamAlias("OutTradeNo")
    @NotBlank
    private String outTradeNo;

    /**
     * 请求时间
     */
    @Size(max = 32)
    @XStreamAlias("RequestTime")
    @NotBlank
    private String requestTime = Dates.format(new Date(),"yyyyMMddHHmmss");
}
