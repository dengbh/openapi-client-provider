package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankHeadResponse;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@XStreamAlias("response")
public class WsBankBkCloudFundsVostroCreateOrderResponseInfo implements Serializable {

	private static final long serialVersionUID = 5629652257836607551L;
	/**
     *响应报文头
     */
    @XStreamAlias("head")
    private WsbankHeadResponse headResponse;

    /**
     * 响应报文体
     */
    @XStreamAlias("body")
    private WsBankBkCloudFundsVostroCreateOrderResponseBody responseBody;
}
