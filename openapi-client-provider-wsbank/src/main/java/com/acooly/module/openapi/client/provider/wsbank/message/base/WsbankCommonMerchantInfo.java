package com.acooly.module.openapi.client.provider.wsbank.message.base;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author weili 2018/5/22 18:14
 */
@Getter
@Setter
public class WsbankCommonMerchantInfo implements Serializable {

	/**
	 * 商户名称。有营业执照的，要求与营业执照上的名称一致。
	 */
	@NotBlank
	@XStreamAlias("MerchantName")
	@Size(max = 256)
	private String merchantName;

	/**
	 * 商户号。网商为商户分配的商户号，通过商户入驻结果查询接口获取。
	 */
	@Size(max = 32)
	@NotBlank
	@XStreamAlias("MerchantId")
	private String merchantId;

	/**
	 * 商户类型。可选值： 01:自然人 02:个体工商户 03:企业商户
	 */
	@Size(max = 8)
	@NotBlank
	@XStreamAlias("MerchantType")
	private String merchantType;

	/**
	 * 商户经营类型。可选值： 01:实体特约商户 02:网络特约商户 03:实体兼网络特约商户
	 */
	@Size(max = 8)
	@NotBlank
	@XStreamAlias("DealType")
	private String dealType;

	/**
	 * 商户清算资金是否支持T+0到账。可选值： Y：支持 N：不支持
	 */
	@Size(max = 8)
	@NotBlank
	@XStreamAlias("SupportPrepayment")
	private String supportPrepayment;

	/**
	 * 结算方式。商户清算资金结算方式，可选值： 01：结算到他行卡 02：结算到余利宝 03：结算到活期户（暂不开放） 04：自提打款（暂不开放）
	 */
	@Size(max = 8)
	@NotBlank
	@XStreamAlias("SettleMode")
	private String settleMode;

	/**
	 * 经营类目。参见附录的经营类目上送。
	 */
	@Size(max = 8)
	@NotBlank
	@XStreamAlias("Mcc")
	private String mcc;

	/**
	 * 商户详情，json格式base64编码，具体报文定义参考下面的商户详情
	 */
	@NotBlank
	@XStreamAlias("MerchantDetail")
	private String merchantDetail;

	/**
	 * 支持交易类型列表。该商户能支持的交易类型，多个用逗号隔开。可选值： 01：正扫交易 02：反扫交易 06：退款交易
	 */
	@NotBlank
	@XStreamAlias("TradeTypeList")
	private String tradeTypeList;

	/**
	 * 支持支付渠道列表。该商户能支持的第三方支付渠道。多个用逗号隔开。可选值： 01：支付宝 02：微信支付 03：手机QQ（暂未开放）
	 * 04：京东钱包（暂未开放）
	 */
	@NotBlank
	@XStreamAlias("PayChannelList")
	private String payChannelList;

	/**
	 * 禁用支付方式。商户禁受理支付方式列表，多个用逗号隔开。可选值： 02：信用卡 03：花呗（仅支付宝）
	 */
	@XStreamAlias("DeniedPayToolList")
	private String deniedPayToolList;

	/**
	 * 手续费列表
	 */
	@NotBlank
	@XStreamAlias("FeeParamList")
	private String feeParamList;

	/**
	 * 清算卡参数。json格式base64编码，具体报文定义参考下面的清算卡，仅结算方式为“01结算到他行卡”需要填写
	 */
	@XStreamAlias("BankCardParam")
	private String bankCardParam;

	/**
	 * 该字段用于决定商户是否可自主选择让其买家使用花呗分期，包括商户承担手续费和用户承担手续费两种模式。目前暂时仅支持用户承担手续费模式。可选值：
	 * 01：支持 02：不支持(默认值)。
	 */
	@Size(max = 2)
	@XStreamAlias("SupportStage")
	private String supportStage;

	/**
	 * 标识进驻的微信渠道号、进驻结果、微信子商户号信息、进驻失败原因 注：该字段2017.11月16日上线
	 */
	@NotBlank
	@Size(max = 64)
	@XStreamAlias("WechatChannelList")
	private String wechatChannelList;

	/**
	 * 支付宝线下进驻后的Smid
	 */
	@NotBlank
	@Size(max = 128)
	@XStreamAlias("Smid")
	private String smid;

	/**
	 * 线上支付宝进驻后的子商户号
	 */
	@NotBlank
	@Size(max = 128)
	@XStreamAlias("OnlineSmid")
	private String onlineSmid;

	/**
	 * 用于记录线下商户由哪一个渠道接入支付宝的，该字段用于营销激励、风控等方面。
	 */
	@NotBlank
	@XStreamAlias("AlipaySource")
	@Size(max = 32)
	private String alipaySource;

	/**
	 * 用于记录线上商户由哪一个渠道接入支付宝的，该字段用于营销激励、风控等方面。
	 */
	@NotBlank
	@Size(max = 32)
	@XStreamAlias("AlipayOnlineSource")
	private String alipayOnlineSource;

	/**
	 * 支付宝线上经营类目。参见附录的支付宝线上经营类目。
	 */
	@NotBlank
	@Size(max = 4)
	@XStreamAlias("OnlineMcc")
	private String onlineMcc;

	/**
	 * 支付宝线上站点信息
	 */
	@NotBlank
	@XStreamAlias("SiteInfo")
	private String siteInfo;
	
	 @Override
	public String toString() {
	        return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
	
}
