package com.acooly.module.openapi.client.provider.yinsheng;

import com.acooly.module.openapi.client.api.notify.ApiServiceClientServlet;
import com.acooly.module.openapi.client.api.transport.HttpTransport;
import com.acooly.module.openapi.client.api.transport.Transport;
import com.acooly.module.openapi.client.provider.yinsheng.enums.YinShengServiceEnum;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.util.List;

import static com.acooly.module.openapi.client.provider.yinsheng.OpenAPIClientYinShengProperties.PREFIX;

@EnableConfigurationProperties({OpenAPIClientYinShengProperties.class})
@ConditionalOnProperty(value = PREFIX + ".enable", matchIfMissing = true)
@ComponentScan
public class OpenAPIClientYinShengConfigration {

    @Autowired
    private OpenAPIClientYinShengProperties openAPIClientYinShengProperties;

    @Bean("yinShengHttpTransport")
    public Transport yinShengHttpTransport() {
        HttpTransport httpTransport = new HttpTransport();
        httpTransport.setGateway(openAPIClientYinShengProperties.getGatewayUrl());
        httpTransport.setConnTimeout(String.valueOf(openAPIClientYinShengProperties.getConnTimeout()));
        httpTransport.setReadTimeout(String.valueOf(openAPIClientYinShengProperties.getReadTimeout()));
        return httpTransport;
    }

    /**
     * 易行通SDK-servlet注册
     *
     * @return
     */
    @Bean
    public ServletRegistrationBean getApiSDKServlet() {
        ServletRegistrationBean bean = new ServletRegistrationBean();
        ApiServiceClientServlet apiServiceClientServlet = new ApiServiceClientServlet();
        bean.setServlet(apiServiceClientServlet);
        bean.addInitParameter(ApiServiceClientServlet.NOTIFY_DISPATCHER_BEAN_NAME_KEY, "yinShengNotifyHandlerDispatcher");
        bean.addInitParameter(ApiServiceClientServlet.SUCCESS_RESPONSE_BODY_KEY, "success");
        List<String> urlMappings = Lists.newArrayList();
        //网关异步通知地址
        urlMappings.add("/gateway/notify/ysNotify/" + YinShengServiceEnum.NETBANK_DEPOSIT.getKey());//访问，可以添加多个
        urlMappings.add("/gateway/notify/ysNotify/" + YinShengServiceEnum.FAST_PAY.getKey());//访问，可以添加多个
        urlMappings.add("/gateway/notify/ysNotify/" + YinShengServiceEnum.SCAN_PAY.getKey());//访问，可以添加多个
        urlMappings.add("/gateway/notify/ysNotify/" + YinShengServiceEnum.WECHAT_JSPAY.getKey());//访问，可以添加多个
        urlMappings.add("/gateway/notify/ysNotify/" + YinShengServiceEnum.APP_PAY.getKey());//访问，可以添加多个
        urlMappings.add("/gateway/notify/ysNotify/" + YinShengServiceEnum.WITHDRAW_D0.getKey());//访问，可以添加多个
        bean.setUrlMappings(urlMappings);
        bean.setLoadOnStartup(3);
        return bean;
    }
}
