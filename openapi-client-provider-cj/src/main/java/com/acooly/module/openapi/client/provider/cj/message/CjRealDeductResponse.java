package com.acooly.module.openapi.client.provider.cj.message;


import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.cj.domain.CjApiMsgInfo;
import com.acooly.module.openapi.client.provider.cj.domain.CjResponse;
import com.acooly.module.openapi.client.provider.cj.enums.CjServiceEnum;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@CjApiMsgInfo(service = CjServiceEnum.CJ_REAL_DEDUCT, type = ApiMessageType.Response)
public class CjRealDeductResponse extends CjResponse {

    /**
     * 交易类型
     */
    String bizType;



}
