package com.acooly.openapi.client.provider.newyl;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Dates;
import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.newyl.NewYlApiService;
import com.acooly.module.openapi.client.provider.newyl.message.NewYlBatchDeductRequest;
import com.acooly.module.openapi.client.provider.newyl.message.NewYlBatchDeductResponse;
import com.acooly.module.openapi.client.provider.newyl.message.NewYlDeductQueryRequest;
import com.acooly.module.openapi.client.provider.newyl.message.NewYlDeductQueryResponse;
import com.acooly.module.openapi.client.provider.newyl.message.NewYlRealDeductRequest;
import com.acooly.module.openapi.client.provider.newyl.message.NewYlRealDeductResponse;
import com.acooly.module.openapi.client.provider.newyl.message.xStream.batchDeduct.request.BdReqBody;
import com.acooly.module.openapi.client.provider.newyl.message.xStream.batchDeduct.request.BdReqTransDetail;
import com.acooly.module.openapi.client.provider.newyl.message.xStream.batchDeduct.request.BdReqTransDetails;
import com.acooly.module.openapi.client.provider.newyl.message.xStream.batchDeduct.request.BdReqTransSum;
import com.acooly.module.openapi.client.provider.newyl.message.xStream.common.ReqInfo;
import com.acooly.module.openapi.client.provider.newyl.message.xStream.deductQuery.request.QdReqBody;
import com.acooly.module.openapi.client.provider.newyl.message.xStream.deductQuery.request.QdReqQueryDetail;
import com.acooly.module.openapi.client.provider.newyl.message.xStream.deductQuery.request.QdReqQueryDetails;
import com.acooly.module.openapi.client.provider.newyl.message.xStream.deductQuery.request.QdReqQueryTrans;
import com.acooly.module.openapi.client.provider.newyl.message.xStream.realDeduct.request.RdReqBody;
import com.acooly.module.openapi.client.provider.newyl.message.xStream.realDeduct.request.RdReqTransDetail;
import com.acooly.module.openapi.client.provider.newyl.message.xStream.realDeduct.request.RdReqTransDetails;
import com.acooly.module.openapi.client.provider.newyl.message.xStream.realDeduct.request.RdReqTransSum;
import com.acooly.test.NoWebTestBase;
import com.alibaba.fastjson.JSON;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@SpringBootApplication
@BootApp(sysName = "YlTest")
public class NewYlTest extends NoWebTestBase {
    @Autowired
    private NewYlApiService newYlApiService;

    String oid = Ids.gid();

    /**
     * BankDeduct:银联单笔代扣
     */
    @Test
    public void testRealDeduct(){

        ReqInfo reqInfo = new ReqInfo();
        reqInfo.setTrxCode("100004");
        reqInfo.setVersion("05");
        reqInfo.setDataType("2");
        reqInfo.setLevel("0");
        reqInfo.setReqSn(Ids.oid());
        reqInfo.setUserName("21005810");
        reqInfo.setUserPass("111111");

        RdReqBody rdReqBody = new RdReqBody();

        RdReqTransSum rdReqTransSum = new RdReqTransSum();
        rdReqTransSum.setTotalItem("1");
        rdReqTransSum.setTotalSum("40000");
        rdReqTransSum.setBusinessCode("14900");
        rdReqTransSum.setMerchantId("001053110000001");
        rdReqTransSum.setSubmitTime(Dates.format(new Date(),"yyyyMMddHHmmss"));

        RdReqTransDetails rdReqTransDetails = new RdReqTransDetails();

        List<RdReqTransDetail> rdReqTransDetailList = new LinkedList<RdReqTransDetail>();
        RdReqTransDetail rdReqTransDetail = new RdReqTransDetail();

        rdReqTransDetail.setSn("1");
        rdReqTransDetail.setBankCode("307");
        rdReqTransDetail.setAccountType("00");
        rdReqTransDetail.setAccountNo("6217003760109841230");
        rdReqTransDetail.setAccountName("傅枫");
        rdReqTransDetail.setProvince("重庆");
        rdReqTransDetail.setBankName("平安银行");
        rdReqTransDetail.setAccountProp("0");
        rdReqTransDetail.setAmount("40000");
        rdReqTransDetail.setIdType("00");
        rdReqTransDetail.setId("500223199111086316");


        rdReqTransDetailList.add(rdReqTransDetail);

        rdReqTransDetails.setTransDetail(rdReqTransDetailList);
        rdReqBody.setTransSum(rdReqTransSum);
        rdReqBody.setTransDetails(rdReqTransDetails);

        NewYlRealDeductRequest request = new NewYlRealDeductRequest();
        request.setPartnerId("001053110000001");
        request.setReqInfo(reqInfo);
        request.setRdReqBody(rdReqBody);

        try {
            NewYlRealDeductResponse response = newYlApiService.newYlRealDeduct(request);
            System.out.println("银联单笔代扣测试执行结果：" + JSON.toJSONString(response));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("异常原因：" + e.getMessage());
        }
    }


    /**
     * BankDeduct:银联批量代扣
     */
    @Test
    public void testBatchDeduct(){

        ReqInfo reqInfo = new ReqInfo();
        reqInfo.setTrxCode("100001");
        reqInfo.setVersion("05");
        reqInfo.setDataType("2");
        reqInfo.setLevel("5");
        reqInfo.setReqSn(Ids.oid());
        reqInfo.setUserName("21005810");
        reqInfo.setUserPass("111111");
        BdReqBody bdReqBody = new BdReqBody();
        BdReqTransSum bdReqTransSum = new BdReqTransSum();
        bdReqTransSum.setBusinessCode("14900");
        bdReqTransSum.setMerchantId("000000000100323");
        bdReqTransSum.setSubmitTime(Dates.format(new Date(),"yyyyMMddHHmmss"));
        bdReqTransSum.setTotalItem("2");
        bdReqTransSum.setTotalSum("80000");

        BdReqTransDetails bdReqTransDetails = new BdReqTransDetails();
        List<BdReqTransDetail> BdReqTransDetailList = new LinkedList<>();

        for(int i = 0; i<2 ; i++){
            BdReqTransDetail bdReqTransDetail = new BdReqTransDetail();
            bdReqTransDetail.setSn("000"+(i+1));
            bdReqTransDetail.setBankCode("102");
            bdReqTransDetail.setAccountType("00");
            bdReqTransDetail.setAccountNo("6217003760109841234");
            bdReqTransDetail.setAccountName("傅枫");
            bdReqTransDetail.setProvince("重庆");
            bdReqTransDetail.setCity("重庆");
            bdReqTransDetail.setBankName("平安银行");
            bdReqTransDetail.setAccountProp("0");
            bdReqTransDetail.setAmount("40000");
            bdReqTransDetail.setIdType("00");
            bdReqTransDetail.setId("500223199111086316");
            bdReqTransDetail.setTel("18288893996");
            bdReqTransDetail.setCurrency("CNY");
            BdReqTransDetailList.add(bdReqTransDetail);
        }

        bdReqTransDetails.setTransDetail(BdReqTransDetailList);
        bdReqBody.setTransSum(bdReqTransSum);
        bdReqBody.setTransDetails(bdReqTransDetails);
        NewYlBatchDeductRequest request = new NewYlBatchDeductRequest();
        request.setReqInfo(reqInfo);

        request.setBdReqBody(bdReqBody);

        try {
            NewYlBatchDeductResponse response = newYlApiService.newYlBatchDeduct(request);
            System.out.println("银联批量代扣测试执行结果：" + JSON.toJSONString(response));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("异常原因：" + e.getMessage());
        }
    }


    /**
     * 银联代扣查询
     */
    @Test
    public void testDeductQuery(){

        NewYlDeductQueryRequest request = new NewYlDeductQueryRequest();
        ReqInfo reqInfo = new ReqInfo();
        reqInfo.setTrxCode("200001");
        reqInfo.setVersion("05");
        reqInfo.setDataType("2");
        reqInfo.setLevel("0");
        reqInfo.setReqSn(Ids.oid());
        reqInfo.setUserName("21005810");
        reqInfo.setUserPass("111111");

        QdReqBody qdReqBody = new QdReqBody();
        QdReqQueryTrans qdReqQueryTrans = new QdReqQueryTrans();

        qdReqQueryTrans.setMerchantId("000000000100323");
        QdReqQueryDetails qdReqQueryDetails = new QdReqQueryDetails();
        List<QdReqQueryDetail> detailList = new LinkedList<QdReqQueryDetail>();
        QdReqQueryDetail detail = new QdReqQueryDetail();
        qdReqQueryTrans.setQuerySn("o18020821144323510001");
        detail.setQueryDetailSn("1");
        detailList.add(detail);
        qdReqQueryDetails.setQueryDetail(detailList);
        qdReqBody.setQueryDetails(qdReqQueryDetails);
        qdReqBody.setQueryTrans(qdReqQueryTrans);

        request.setReqInfo(reqInfo);
        request.setQdReqBody(qdReqBody);
        try {
            NewYlDeductQueryResponse response = newYlApiService.newYlDeductQuery(request);
                System.out.println("银联查询：" + JSON.toJSONString(response));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("异常原因：" + e.getMessage());
        }
    }
}