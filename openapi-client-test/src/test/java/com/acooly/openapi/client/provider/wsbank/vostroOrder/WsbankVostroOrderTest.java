package com.acooly.openapi.client.provider.wsbank.vostroOrder;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.wsbank.WsbankApiService;
import com.acooly.module.openapi.client.provider.wsbank.enums.WsbankPayeeTypeEnum;
import com.acooly.module.openapi.client.provider.wsbank.message.WsBankBkCloudFundsVostroCreateOrderRequest;
import com.acooly.module.openapi.client.provider.wsbank.message.WsBankBkCloudFundsVostroCreateOrderResponse;
import com.acooly.module.openapi.client.provider.wsbank.message.WsbankBkCloudFundsVostroQueryOrderRequest;
import com.acooly.module.openapi.client.provider.wsbank.message.WsbankBkCloudFundsVostroQueryOrderResponse;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsBankBkCloudFundsVostroCreateOrderRequestBody;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsBankBkCloudFundsVostroCreateOrderRequestInfo;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankBkCloudFundsVostroQueryOrderRequestBody;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankBkCloudFundsVostroQueryOrderRequestInfo;
import com.acooly.test.NoWebTestBase;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 来帐汇入订单创建
 * @author sunjx
 *
 */
@SpringBootApplication
@BootApp(sysName = "wsbankTest")
public class WsbankVostroOrderTest extends NoWebTestBase {
    @Autowired
    private WsbankApiService wsbankApiService;
    
    /**
     * 测试来帐汇入订单创建
     */
    @Test
    public void createVostroOrderTest() {
    	WsBankBkCloudFundsVostroCreateOrderRequestBody requestBody = new WsBankBkCloudFundsVostroCreateOrderRequestBody();
    	requestBody.setMerchantId("226801000000114869124");//商户号
    	requestBody.setPayeeId("226801000000114869124");//收款方Id
    	requestBody.setPayeeType(WsbankPayeeTypeEnum.MERCHANT.code());//收款方类型
    	requestBody.setOutTradeNo(Ids.oid());//外部请求流水号
    	requestBody.setTotalAmount("1");//订单金额
    	requestBody.setBody("测试商品");//商品描述
        WsBankBkCloudFundsVostroCreateOrderRequestInfo requestInfo = new WsBankBkCloudFundsVostroCreateOrderRequestInfo();
        requestInfo.setRequestBody(requestBody);
        WsBankBkCloudFundsVostroCreateOrderRequest request = new WsBankBkCloudFundsVostroCreateOrderRequest();
        request.setRequestInfo(requestInfo);
        WsBankBkCloudFundsVostroCreateOrderResponse response = wsbankApiService.vostroCreateOrder(request);
        System.out.println("来帐汇入订单创建："+ JSON.toJSONString(response));
    }
    
    /**
     * 单笔来帐汇入订单查询
     */
    @Test
    public void searchVostroOrderTest() {
    	WsbankBkCloudFundsVostroQueryOrderRequestBody requestBody = new WsbankBkCloudFundsVostroQueryOrderRequestBody();
    	requestBody.setMerchantId("226801000000114869124");
    	requestBody.setOutTradeNo("o18070516480099960001");
    	requestBody.setOrderNo("201807120500015656");
    	WsbankBkCloudFundsVostroQueryOrderRequestInfo requestInfo = new WsbankBkCloudFundsVostroQueryOrderRequestInfo();
        requestInfo.setRequestBody(requestBody);
        WsbankBkCloudFundsVostroQueryOrderRequest request = new WsbankBkCloudFundsVostroQueryOrderRequest();
        request.setRequestInfo(requestInfo);
        WsbankBkCloudFundsVostroQueryOrderResponse response = wsbankApiService.vostroQueryOrder(request);
        System.out.println("单笔来帐汇入订单查询："+ JSON.toJSONString(response));
    }
    
}
