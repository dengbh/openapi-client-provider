package com.acooly.module.openapi.client.provider.yuejb.domain;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import lombok.Data;

/**
 * Created by ouwen@yiji.com} on 2017/11/6.
 */
@Data
public class YueJBResponse extends YueJBApiMessage {
    @ApiItem(sign = false)
    private String code;
    /**
     * 签名串
     */
    private String sign;
    /**
     * 商户订单号
     */
    private String merchOrderNo;
    /**
     * 	签名方式
     */
    private String signType;
    /**
     * 响应消息
     */
    private String message;
    /**
     * 服务版本
     */
    private String version;
    /**
     * 成功失败标识
     */
    private String status;
    /**
     * 异常描述
     */
    private String description;
    /**
     *会话参数
     */
    @ApiItem(sign = false)
    private String context;
}
