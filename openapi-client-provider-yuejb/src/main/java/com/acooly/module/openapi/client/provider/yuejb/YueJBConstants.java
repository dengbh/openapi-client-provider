/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-19 18:12 创建
 */
package com.acooly.module.openapi.client.provider.yuejb;

/**
 Created by ouwen@yiji.com} on 2017/11/6.
 */
public class YueJBConstants {

    public static final String PROVIDER_NAME = "yuejb";

    public static final String SIGN = "sign";

    public static final String SIGN_TYPE = "yuejb";
    /**
     * 额度冻结
     */
    public static final String FREEZE = "freezeQuotarating";
    /**
     * 放款
     */
    public static final String LOAN = "sellOnCreditLend";
}
