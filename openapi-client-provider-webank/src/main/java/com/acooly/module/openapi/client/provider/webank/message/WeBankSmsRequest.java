package com.acooly.module.openapi.client.provider.webank.message;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.webank.domain.WeBankApiMsgInfo;
import com.acooly.module.openapi.client.provider.webank.domain.WeBankRequest;
import com.acooly.module.openapi.client.provider.webank.enums.WeBankServiceEnum;
import com.acooly.module.openapi.client.provider.webank.message.dto.WeBankSmsInfo;

import lombok.Getter;
import lombok.Setter;

/**
 * @author fufeng
 */
@Getter
@Setter
@WeBankApiMsgInfo(service = WeBankServiceEnum.WEBANK_SMS, type = ApiMessageType.Request)
public class WeBankSmsRequest extends WeBankRequest {

    /**
     * 快捷申请业务信息
     */
    @ApiItem(value = "content")
    private WeBankSmsInfo weBankSmsInfo;
}
