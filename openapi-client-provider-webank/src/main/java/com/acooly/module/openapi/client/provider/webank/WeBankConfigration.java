package com.acooly.module.openapi.client.provider.webank;

import com.google.common.collect.Lists;

import com.acooly.module.openapi.client.api.notify.ApiServiceClientServlet;
import com.acooly.module.openapi.client.api.transport.HttpTransport;
import com.acooly.module.openapi.client.api.transport.Transport;
import com.acooly.module.openapi.client.provider.webank.enums.WeBankServiceEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import static com.acooly.module.openapi.client.provider.webank.WeBankProperties.PREFIX;


@EnableConfigurationProperties({WeBankProperties.class})
@ConditionalOnProperty(value = PREFIX + ".enable", matchIfMissing = true)
@ComponentScan
public class WeBankConfigration {

    @Autowired
    private WeBankProperties weBankProperties;

    @Bean("weBankHttpTransport")
    public Transport weBankHttpTransport() {
        HttpTransport httpTransport = new HttpTransport();
        httpTransport.setGateway(weBankProperties.getGatewayUrl());
        httpTransport.setConnTimeout(String.valueOf(weBankProperties.getConnTimeout()));
        httpTransport.setReadTimeout(String.valueOf(weBankProperties.getReadTimeout()));
        return httpTransport;
    }

    /**
     * 接受异步通知的servlet
     *
     * @return
     */
    @Bean
    public ServletRegistrationBean weBankApiServiceClientServlet() {
        ServletRegistrationBean bean = new ServletRegistrationBean();
        ApiServiceClientServlet apiServiceClientServlet = new ApiServiceClientServlet();
        bean.setServlet(apiServiceClientServlet);
        bean.addInitParameter(ApiServiceClientServlet.NOTIFY_DISPATCHER_BEAN_NAME_KEY, "weBankNotifyHandlerDispatcher");
        //返回的信息
        bean.addInitParameter(ApiServiceClientServlet.SUCCESS_RESPONSE_BODY_KEY, "success");
        bean.setUrlMappings(Lists.newArrayList("/gateway/notify/wzNotify/"+ WeBankServiceEnum.WEBANK_DEDUCT.code()));
        bean.setUrlMappings(Lists.newArrayList("/gateway/notify/wzNotify"+ WeBankServiceEnum.WEBANK_WITHDRAW.code()));
        bean.setLoadOnStartup(3);
        return bean;
    }

}
