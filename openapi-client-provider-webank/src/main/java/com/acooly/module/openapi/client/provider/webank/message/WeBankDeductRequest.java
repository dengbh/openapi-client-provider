package com.acooly.module.openapi.client.provider.webank.message;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.webank.domain.WeBankApiMsgInfo;
import com.acooly.module.openapi.client.provider.webank.domain.WeBankRequest;
import com.acooly.module.openapi.client.provider.webank.enums.WeBankServiceEnum;
import com.acooly.module.openapi.client.provider.webank.message.dto.WeBankDeductInfo;

import lombok.Getter;
import lombok.Setter;

/**
 * @author fufeng
 */
@Getter
@Setter
@WeBankApiMsgInfo(service = WeBankServiceEnum.WEBANK_DEDUCT, type = ApiMessageType.Request)
public class WeBankDeductRequest extends WeBankRequest {

    /**
     * 代扣业务信息
     */
    @ApiItem(value = "content")
    private WeBankDeductInfo weBankDeductInfo;
}
