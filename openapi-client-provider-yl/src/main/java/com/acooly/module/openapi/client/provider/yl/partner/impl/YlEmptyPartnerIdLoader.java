/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-11-15 13:42 创建
 */
package com.acooly.module.openapi.client.provider.yl.partner.impl;


import com.acooly.module.openapi.client.provider.yl.YlProperties;
import com.acooly.module.openapi.client.provider.yl.domain.YlResponse;
import com.acooly.module.openapi.client.provider.yl.partner.YlPartnerIdLoadManager;


import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
 * 此为获取partnerId的空实现 （demo）
 *
 * @author zhangpu 2017-11-15 13:42
 */
@Slf4j
@Service("ylPartnerIdLoadManager")
public class YlEmptyPartnerIdLoader implements YlPartnerIdLoadManager {

    @Autowired
    private YlProperties ylProperties;

    @Override
    public String load(YlResponse ylResponse) {
        String partnerId = "";
        if(StringUtils.isBlank(ylResponse.getBatchNo())){
            //如果是批量业务，查询此批次的第一条流水的partnerId
        }else{

        }
        return ylProperties.getPartnerId();
    }
}
