package com.acooly.module.openapi.client.provider.shengpay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayApiMsg;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayResponse;
import com.acooly.module.openapi.client.provider.shengpay.enums.ShengpayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/5/17 18:57
 */
@Getter
@Setter
@ShengpayApiMsg(service = ShengpayServiceNameEnum.CREATE_PAYMENT_ORDER,type = ApiMessageType.Response)
public class ShengpayCreatePaymentOrderResponse extends ShengpayResponse {

    /**
     * 商户系统内的唯一订单号
     */
    @NotBlank
    @Size(max = 32)
    private String merchantOrderNo;

    /**
     * 盛付通系统内针对此商户订单的唯一订单号，如: 20160105105839885474
     */
    private String sftOrderNo;

    /**
     * 订单创建时间，格式：yyyyMMddHHmmss 如：20160105105839
    */
    private String orderCreateTime;

    /**
     * 支付token，用于后续的支付预校验和支付确认
     */
    private String sessionToken;
}
