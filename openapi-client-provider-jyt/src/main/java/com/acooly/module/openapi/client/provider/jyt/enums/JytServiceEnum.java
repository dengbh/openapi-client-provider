/*
 * ouwen@yiji.com Inc.
 * Copyright (c) 2017 All Rights Reserved.
 * create by ouwen
 * date:2017-03-30
 *
 */
package com.acooly.module.openapi.client.provider.jyt.enums;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * inst_channel_api DebitCreditType 枚举定义
 *
 * @author zhike
 * Date: 2017-03-30 15:12:06
 */
public enum JytServiceEnum implements Messageable {

    DEDUCT_APPLY("deductApply","TD1004", "代扣申请"),
    DEDUCT_PAY("deductPay","TD4005", "代扣充值"),
    RETRIEVES_SMS_CODE("retrievesSmsCode","TD4003", "重新获取验证码"),
    TRADE_ORDER_QUERY("tradeOrderQuery","TD2001", "支付交易单笔订单查询"),
    CHECK_CARD_APPLY("checkBankCardApply","TD1005", "四要素验卡发送短息"),
    CHECK_CARD_CONFIRM("checkBankCardConfirm","TD4006", "四要素验卡确认"),
    UN_BIND_CARD("unBindCard","TD4002", "解除实名支付银行卡"),
    ;

    private final String code;
    private final String key;
    private final String message;

    private JytServiceEnum(String code, String key, String message) {
        this.code = code;
        this.message = message;
        this.key = key;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    public String getKey() {
        return key;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        for (JytServiceEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static JytServiceEnum find(String code) {
        for (JytServiceEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param key 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 key 没有对应的 Status 。
     */
    public static JytServiceEnum findByKey(String key) {
        for (JytServiceEnum status : values()) {
            if (status.getKey().equals(key)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<JytServiceEnum> getAll() {
        List<JytServiceEnum> list = new ArrayList<JytServiceEnum>();
        for (JytServiceEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (JytServiceEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }

}
