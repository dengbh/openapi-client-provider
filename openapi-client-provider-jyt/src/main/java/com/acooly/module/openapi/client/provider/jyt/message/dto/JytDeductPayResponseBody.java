package com.acooly.module.openapi.client.provider.jyt.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author zhike 2018/5/8 15:55
 */
@Getter
@Setter
@XStreamAlias("body")
public class JytDeductPayResponseBody implements Serializable {

    /**
     * 支付状态
     当报文头响应码为S0000000时有值。
     00支付成功
     01平台处理中
     02银行处理中
     03 支付失败
     */
    @XStreamAlias("tran_state")
    private String tranState;

    /**
     * 支付订单号
     */
    @Size(max = 32)
    @XStreamAlias("order_id")
    @NotBlank
    private String orderId;

    /**
     * 交易金额
     * 支付金额(交易终态时，返回)
     */
    @Size(max = 32)
    @XStreamAlias("tran_amount")
    @NotBlank
    private String tranAmount;

    /**
     * 支付订单号
     * 可退款金额(交易终态时，返回)
     */
    @Size(max = 15)
    @XStreamAlias("avl_amount")
    @NotBlank
    private String avlAmount;

    /**
     * 交易时间
     商户交易开始时间，格式为
     YYYYMMDDHHmmSS(交易终态时，返回)
     */
    @Size(max = 14)
    @XStreamAlias("tran_date")
    @NotBlank
    private String tranDate;

    /**
     * 备注信息
     * 结果说明
     */
    @Size(max = 60)
    @XStreamAlias("remark")
    private String remark;
}
