/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年4月1日
 *
 */
package com.acooly.module.openapi.client.provider.allinpay.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.provider.allinpay.domain.AllinpayRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author zhike
 */
@Service
public class AllinpayRequestMarshall extends AllinpayMarshallSupport implements ApiMarshal<String, AllinpayRequest> {

    private static final Logger logger = LoggerFactory.getLogger(AllinpayRequestMarshall.class);

    @Override
    public String marshal(AllinpayRequest source) {
        beforeMarshall(source);
        return doMarshall(source);
    }

}
