/*
 * ouwen@yiji.com Inc.
 * Copyright (c) 2017 All Rights Reserved.
 * create by ouwen
 * date:2017-03-30
 *
 */
package com.acooly.module.openapi.client.provider.allinpay.enums;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * inst_channel_api DebitCreditType 枚举定义
 *
 * @author zhike
 * Date: 2017-03-30 15:12:06
 */
public enum AllinpayIdTypeEnum implements Messageable {

    ID_TYPE_0("0", "身份证"),
    ID_TYPE_1("1", "户口簿"),
    ID_TYPE_2("2", "护照"),
    ID_TYPE_3("3", "军官证"),
    ID_TYPE_4("4", "士兵证"),
    ID_TYPE_5("5", "港澳居民来往内地通行证"),
    ID_TYPE_6("6", "台湾同胞来往内地通行证"),
    ID_TYPE_7("7", "临时身份证"),
    ID_TYPE_8("8", "外国人居留证"),
    ID_TYPE_9("9", "警官证"),
    ID_TYPE_X("X", "其他证件"),
    ;
    private final String code;
    private final String message;

    private AllinpayIdTypeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        for (AllinpayIdTypeEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static AllinpayIdTypeEnum find(String code) {
        for (AllinpayIdTypeEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<AllinpayIdTypeEnum> getAll() {
        List<AllinpayIdTypeEnum> list = new ArrayList<AllinpayIdTypeEnum>();
        for (AllinpayIdTypeEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (AllinpayIdTypeEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }
}
