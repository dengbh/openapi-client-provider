/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-24 19:29 创建
 */
package com.acooly.module.openapi.client.provider.hx.enums;

import com.google.common.collect.Maps;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * BOSC服务名称枚举
 *
 * @author zhangpu 2017-09-24 19:29
 */
public enum HxServiceEnum implements Messageable {

    hxNetbankPay("hxNetbankPay", "环讯网银支付"),
    hxNetbankPayQuery("hxNetbankPayQuery", "环讯网银查询"),
    hxWithdraw("hxWithdraw", "环讯代付"),
    hxWithdrawQuery("hxWithdrawQuery", "环讯代付查询"),
    ;

    private final String code;
    private final String message;

    private HxServiceEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = Maps.newLinkedHashMap();
        for (HxServiceEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static HxServiceEnum find(String code) {
        for (HxServiceEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        throw new IllegalArgumentException("BoscServiceNameEnum not legal:" + code);
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<HxServiceEnum> getAll() {
        List<HxServiceEnum> list = new ArrayList<HxServiceEnum>();
        for (HxServiceEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (HxServiceEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }


}
