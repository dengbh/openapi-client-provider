package com.acooly.module.openapi.client.provider.fuyou.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouResponse;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.acooly.module.openapi.client.provider.fuyou.message.dto.FuyouWithdrawQueryInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author zhike 2018/3/19 15:51
 */
@Getter
@Setter
@FuyouApiMsgInfo(service = FuyouServiceEnum.FUYOU_WITHDRAW_QUERY,type = ApiMessageType.Response)
@XStreamAlias("qrytransrsp")
public class FuyouWithdrawQueryResponse extends FuyouResponse{


    /**
     * 响应码
     */
    @XStreamAlias("ret")
    @Size(max = 6)
    @NotBlank
    private String ret;

    /**
     * 备注
     * 填写后，系统体现在交易查询中
     */
    @XStreamAlias("memo")
    @Size(max = 1024)
    @NotBlank
    private String memo;

    /**
     * 查询响应实体
     */
    @XStreamImplicit(itemFieldName="trans")
    private List<FuyouWithdrawQueryInfo> withdrawQueryInfo;
}
