/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.fuyou.domain;

import com.acooly.module.openapi.client.provider.fuyou.support.FuyouAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhangpu
 */
@Getter
@Setter
public class FuyouRequest extends FuyouApiMessage {

    /**
     * 商户代码
     * 富友分配给各合作商户的唯一识别码
     */
    @FuyouAlias("mchnt_cd")
    @NotBlank
    @Size(max = 15)
    private String mchntCd;

    /**
     * MD5 摘要数据
     */
    @FuyouAlias("md5")
    private String md5;

    /**
     * 请求类型
     */
    @XStreamOmitField
    private String reqtype;
}
