/**
 * create by zhangpu
 * date:2015年3月24日
 */
package com.acooly.module.openapi.client.provider.fuyou.enums;

/**
 * @author zhangpu
 *
 */
public enum FuyouServiceTypeEnum {
	STANDARD("standard", "标准方式"),
	APIFMS("apifms","apifms方式"),
	FMS("fms","fms方式"),
	FM("fm","fm方式"),
	WI("wi","提现方式"),
	;


	private String key;
	private String val;

	private FuyouServiceTypeEnum(String key, String val) {
		this.key = key;
		this.val = val;
	}

	public String getKey() {
		return key;
	}

	public String getVal() {
		return val;
	}

}
