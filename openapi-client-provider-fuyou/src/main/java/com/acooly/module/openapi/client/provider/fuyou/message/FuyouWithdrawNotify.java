package com.acooly.module.openapi.client.provider.fuyou.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouNotify;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.acooly.module.openapi.client.provider.fuyou.support.FuyouAlias;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * 退票通知
 * 系统对账完成后，收付平台生产退票，系统主动通知商户。
 * @author zhike 2018/3/19 15:51
 */
@Getter
@Setter
@FuyouApiMsgInfo(service = FuyouServiceEnum.FUYOU_WITHDRAW,type = ApiMessageType.Notify)
public class FuyouWithdrawNotify extends FuyouNotify{
    /**
     * 商户请求流水
     * 如果商户没传则为空字符串
     */
    @FuyouAlias("orderno")
    @NotBlank
    @Size(max = 30)
    private String orderNo;

    /**
     * 原请求日期
     * 原交易日期
     */
    @FuyouAlias("merdt")
    @NotBlank
    @Size(max = 8)
    private String merDt;

    /**
     * 富友流水
     * 富友生成的原交易流水
     */
    @FuyouAlias("fuorderno")
    @NotBlank
    @Size(max = 30)
    private String fuorderNo;

    /**
     * 银行卡号
     */
    @FuyouAlias("accntno")
    @NotBlank
    private String accntNo;

    /**
     * 银行卡持卡人姓名
     */
    @FuyouAlias("accntnm")
    @NotBlank
    private String realName;

    /**
     * 总行代码
     * 参见总行代码表
     */
    @FuyouAlias("bankno")
    @Size(max = 4)
    @NotBlank
    private String bankNo;

    /**
     * 退票金额
     * 单位：分
     */
    @FuyouAlias("amt")
    @Size(max = 12)
    @NotBlank
    private String amount;

    /**
     * 交易状态
     * 参见交易状态码说明，1 为退票成功
     */
    @FuyouAlias("state")
    @Size(max = 2)
    @NotBlank
    private String state;

    /**
     * 交易结果
     */
    @FuyouAlias("result")
    @Size(max = 8)
    @NotBlank
    private String result;

    /**
     * 结果原因
     */
    @FuyouAlias("reason")
    @Size(max = 1024)
    private String reason;

    /**
     * 校验值
     */
    @FuyouAlias("mac")
    @NotBlank
    private String mac;

    @Override
    public String getSignStr() {
        return getPartner()+"|"+getKey()+"|"+getOrderNo()+"|"+getMerDt()+"|"+getAccntNo()+"|"+getAmount();
    }
}
