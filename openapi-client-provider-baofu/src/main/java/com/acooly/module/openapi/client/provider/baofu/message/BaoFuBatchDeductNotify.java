package com.acooly.module.openapi.client.provider.baofu.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuNotify;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuServiceEnum;
import com.acooly.module.openapi.client.provider.baofu.support.BaoFuAlias;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/1/29 16:12
 */
@Getter
@Setter
@BaoFuApiMsgInfo(service = BaoFuServiceEnum.BATCH_DEDUCT,type = ApiMessageType.Notify)
@XStreamAlias("result")
public class BaoFuBatchDeductNotify extends BaoFuNotify{

    /**
     * 成功金额 单位：分
     */
    @XStreamAlias("succ_amt")
    private String succAmt;
}
