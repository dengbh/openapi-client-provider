/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-22 11:45 创建
 */
package com.acooly.module.openapi.client.provider.bosc.domain;

/**
 * @author zhangpu 2017-09-22 11:45
 */
public class BoscNotify extends BoscResponse {

    private String responseType;

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }
}
