package com.acooly.module.openapi.client.provider.bosc.message.fund;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscResponse;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.message.fund.info.RechargeDetailInfo;

import java.util.List;

@ApiMsgInfo(service = BoscServiceNameEnum.QUERY_TRANSACTION_RECHARGE, type = ApiMessageType.Response)
public class QueryTransactionRechargeResponse extends BoscResponse {
	
	private List<RechargeDetailInfo> records;
	
	public List<RechargeDetailInfo> getRecords () {
		return records;
	}
	
	public void setRecords (
			List<RechargeDetailInfo> records) {
		this.records = records;
	}
}