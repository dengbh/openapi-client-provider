/*
 *
 * www.cutebear.com Inc.
 * Copyright (c) 2017  All Rights Reserved
 */

package com.acooly.module.openapi.client.provider.bosc.enums.member;

import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by liubin@prosysoft.com on 2017/9/27.
 */
public enum BoscAccessTypeEnum {
	
	FULL_CHECKED("FULL_CHECKED","四要素验证通过"),
	
	
	NOT_AUTH("NOT_AUTH","未鉴权"),
	
	
	AUDIT_AUTH("AUDIT_AUTH","特殊用户认证"),
	
	
	PART_CHECKED("PART_CHECKED","企业用户认证");
	
	
	private final String code;
	private final String message;
	
	private BoscAccessTypeEnum (String code, String message) {
		this.code = code;
		this.message = message;
	}
	
	public String getCode() {
		return code;
	}
	
	public String getMessage() {
		return message;
	}
	
	public String code() {
		return code;
	}
	
	public String message() {
		return message;
	}
	
	public static Map<String, String> mapping() {
		Map<String, String> map = Maps.newLinkedHashMap();
		for (BoscAccessTypeEnum type : values()) {
			map.put(type.getCode(), type.getMessage());
		}
		return map;
	}
	
	/**
	 * 通过枚举值码查找枚举值。
	 *
	 * @param code 查找枚举值的枚举值码。
	 * @return 枚举值码对应的枚举值。
	 * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
	 */
	public static BoscAccessTypeEnum find(String code) {
		for (BoscAccessTypeEnum status : values()) {
			if (status.getCode().equals(code)) {
				return status;
			}
		}
		throw new IllegalArgumentException("BoscAuditStatusEnum not legal:" + code);
	}
	
	/**
	 * 获取全部枚举值。
	 *
	 * @return 全部枚举值。
	 */
	public static List<BoscAccessTypeEnum> getAll() {
		List<BoscAccessTypeEnum> list = new ArrayList<BoscAccessTypeEnum> ();
		for (BoscAccessTypeEnum status : values()) {
			list.add(status);
		}
		return list;
	}
	
	/**
	 * 获取全部枚举值码。
	 *
	 * @return 全部枚举值码。
	 */
	public static List<String> getAllCode() {
		List<String> list = new ArrayList<String>();
		for (BoscAccessTypeEnum status : values()) {
			list.add(status.code());
		}
		return list;
	}
		
	}
