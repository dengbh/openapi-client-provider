/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-24 16:11 创建
 */
package com.acooly.module.openapi.client.provider.bosc.marshall;

import com.acooly.core.utils.Encodes;
import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 同步请求报文组装
 *
 * @author zhangpu 2017-09-24 16:11
 */
@Slf4j
@Component
public class BoscRequestMarshall extends BoscMarshallSupport implements ApiMarshal<String, BoscRequest> {

    @Override
    public String marshal(BoscRequest source) {
        return doHttpBodyMarshall(doMarshall(source));
    }


    private String doHttpBodyMarshall(Map<String, String> requestData) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> entry : requestData.entrySet()) {
            sb.append(Strings.trimToEmpty(entry.getKey())).append("=")
                    .append(Encodes.urlEncode(Strings.trimToEmpty(entry.getValue()))).append("&");
        }
        String result = sb.substring(0, sb.length() - 1);
        log.debug("请求报文(发送): {}", result);
        return result;
    }


}
