<form id="redirectForm_${random}" action="${redirectUrl}" method="post">
<#list formDatas?keys as key>
    <input name="${key}" value="${formDatas[key]?default("")}"/>
</#list>
</form>
<script>document.getElementById("redirectForm_${random}").submit();</script>