/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年12月12日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.trade;

import com.acooly.core.utils.Money;
import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.annotation.SinaGateWay;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayGateWayType;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 标的录入 请求报文
 * 
 * @author zhike
 */
@Getter
@Setter
@SinaGateWay(SinapayGateWayType.PAYMENT)
@SinapayApiMsg(service = SinapayServiceNameEnum.CREATE_BID_INFO, type = ApiMessageType.Request)
public class CreateP2pHostingBorrowingTargetRequest extends SinapayRequest {
	/**
	 * 商户标的号
	 *
	 * 商户网站标的号，商户标的相关数据唯一索引
	 */
	@NotEmpty
	@Size(max = 64)
	@ApiItem(value = "goods_id")
	private String goodsId;

	/**
	 * 标的名称
	 *
	 * 标的名称
	 */
	@NotEmpty
	@Size(max = 64)
	@ApiItem(value = "goods_name")
	private String goodsName;

	/**
	 * 年化收益率
	 *
	 * 年化收益率
	 */
	@NotNull
	@ApiItem(value = "annual_yield")
	private Money annualYield;

	/**
	 * 还款期限
	 *
	 * yyyyMMddHHmmss
	 */
	@NotEmpty
	@Size(max = 14)
	@ApiItem(value = "term")
	private String term;

	/**
	 * 还款方式
	 *
	 * DEBT_MATURITY --到期还本付息MONTHLY_PAYMENT --按月付息 到期还本
	 */
	@NotEmpty
	@Size(max = 20)
	@ApiItem(value = "repay_method")
	private String repayMethod = "MONTHLY_PAYMENT";

	/**
	 * 担保方式
	 *
	 * 企业担保
	 */
	@NotEmpty
	@Size(max = 64)
	@ApiItem(value = "guarantee_method")
	private String guaranteeMethod = "企业担保";

	/**
	 * 借款人集合
	 *
	 * 详见“集合参数”。参数间用“^”分隔，各条目之间用“$”分隔
	 */
	@NotEmpty
	@Size(max = 1000)
	@ApiItem(value = "debtor_list")
	private String debtorList;

	/**
	 * 投资人集合
	 *
	 * 同上
	 */
	@Size(max = 1000)
	@ApiItem(value = "investor_list")
	private String investorList;

	/**
	 * 标的总金额
	 *
	 * 本标的的预期总额
	 */
	@NotNull
	@ApiItem(value = "total_amount")
	private Money totalAmount;

	/**
	 * 标的开始时间
	 *
	 * yyyyMMddHHmmss
	 */
	@NotEmpty
	@Size(max = 14)
	@ApiItem(value = "begin_date")
	private String beginDate;

	/**
	 * 标的url
	 *
	 * 标的url
	 */
	@NotEmpty
	@Size(max = 1000)
	@ApiItem(value = "url")
	private String url;

	/**
	 * 摘要
	 */
	@Size(max = 200)
	@ApiItem(value = "summary")
	private String summary;
}
