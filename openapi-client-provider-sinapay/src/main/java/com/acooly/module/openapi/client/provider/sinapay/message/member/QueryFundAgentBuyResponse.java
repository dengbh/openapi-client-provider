package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayResponse;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author zhike 2018/7/10 15:29
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.QUERY_FUND_AGENT_BUY, type = ApiMessageType.Response)
public class QueryFundAgentBuyResponse extends SinapayResponse {

    /**
     *经办人编号
     */
    @NotEmpty
    @ApiItem(value = "agent_id")
    private String agentId;

    /**
     *用户会员编号
     */
    @NotEmpty
    @ApiItem(value = "member_id")
    private String memberId;


    /**
     *经办人电子邮箱
     */
    @ApiItem(value = "email")
    private String email;

    /**
     *经办人姓名
     */
    @ApiItem(value = "agent_name")
    private String agentName;

    /**
     *经办人手机号
     */
    @ApiItem(value = "agent_mobile")
    private String agentMobile;

    /**
     *证件号码
     */
    @ApiItem(value = "license_no")
    private String licenseNo;

    /**
     *证件类型
     * 证件类型（ID：身份证
     * PASSPORT：护照
     * CHINESE_MAINLAND_PASS：港澳通行证）
     */
    @ApiItem(value = "license_type_code")
    private String licenseTypeCode;

    /**
     *平台
     * 平台（POSS, EWallet,UNKNOWN, Wallet,OPENAPI,MGS）
     */
    @ApiItem(value = "platform")
    private String platform;

    /**
     *商户IP
     */
    @ApiItem(value = "cip")
    private String cip;

    /**
     *状态
     */
    @ApiItem(value = "status")
    private String status;

    /**
     *创建日期
     * yyyyMMddHHmmss
     */
    @ApiItem(value = "create_date")
    private String createDate;

}
