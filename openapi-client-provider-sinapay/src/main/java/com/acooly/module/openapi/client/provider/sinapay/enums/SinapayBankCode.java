/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月1日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.enums;

import com.acooly.core.utils.enums.Messageable;
import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author zhike
 */
public enum SinapayBankCode implements Messageable {

	SINAPAY("SINAPAY", "新浪支付"),

	ABC("ABC", "农业银行"),

	BCCB("BCCB", "北京银行"),

	BJRCB("BJRCB", "北京农商行"),

	BOC("BOC", "中国银行"),

	BOS("BOS", "上海银行"),

	CBHB("CBHB", "渤海银行"),

	CCB("CCB", "建设银行"),

	CCQTGB("CCQTGB", "重庆三峡银行"),

	CEB("CEB", "光大银行"),

	CIB("CIB", "兴业银行"),

	CITIC("CITIC", "中信银行"),

	CMB("CMB", "招商银行"),

	CMBC("CMBC", "民生银行"),

	COMM("COMM", "交通银行"),

	CSCB("CSCB", "长沙银行"),

	CZB("CZB", "浙商银行"),

	CZCB("CZCB", "浙江稠州商业银行"),

	GDB("GDB", "广东发展银行"),

	GNXS("GNXS", "广州市农信社"),

	GZCB("GZCB", "广州市商业银行"),

	HCCB("HCCB", "杭州银行"),

	HKBCHINA("HKBCHINA", "汉口银行"),

	HSBANK("HSBANK", "徽商银行"),

	HXB("HXB", "华夏银行"),

	ICBC("ICBC", "工商银行"),

	NBCB("NBCB", "宁波银行"),

	NJCB("NJCB", "南京银行"),

	PSBC("PSBC", "中国邮储银行"),

	SHRCB("SHRCB", "上海农村商业银行"),

	SNXS("SNXS", "深圳农村商业银行"),

	SPDB("SPDB", "浦东发展银行"),

	SXJS("SXJS", "晋城市商业银行"),

	SZPAB("SZPAB", "平安银行"),

	UPOP("UPOP", "银联在线支付"),

	WZCB("WZCB", "温州市商业银行");

	private final String code;
	private final String message;

	private SinapayBankCode(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public String code() {
		return code;
	}

	public String message() {
		return message;
	}

	public static Map<String, String> mapping() {
		Map<String, String> map = Maps.newLinkedHashMap();
		for (SinapayBankCode type : values()) {
			map.put(type.getCode(), type.getMessage());
		}
		return map;
	}

	/**
	 * 通过枚举值码查找枚举值。
	 * 
	 * @param code
	 *            查找枚举值的枚举值码。
	 * @return 枚举值码对应的枚举值。
	 * @throws IllegalArgumentException
	 *             如果 code 没有对应的 Status 。
	 */
	public static SinapayBankCode find(String code) {
		for (SinapayBankCode status : values()) {
			if (status.getCode().equals(code)) {
				return status;
			}
		}
		throw new IllegalArgumentException("SinapayBankCode not legal:" + code);
	}

	/**
	 * 获取全部枚举值。
	 * 
	 * @return 全部枚举值。
	 */
	public static List<SinapayBankCode> getAll() {
		List<SinapayBankCode> list = new ArrayList<SinapayBankCode>();
		for (SinapayBankCode status : values()) {
			list.add(status);
		}
		return list;
	}

	/**
	 * 获取全部枚举值码。
	 * 
	 * @return 全部枚举值码。
	 */
	public static List<String> getAllCode() {
		List<String> list = new ArrayList<String>();
		for (SinapayBankCode status : values()) {
			list.add(status.code());
		}
		return list;
	}

	@Override
	public String toString() {
		return String.format("%s:%s", this.code, this.message);
	}

}
