package com.acooly.module.openapi.client.provider.sinapay.message.trade;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayResponse;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;

@SinapayApiMsg(service = SinapayServiceNameEnum.FINISH_PRE_AUTH_TRADE, type = ApiMessageType.Response)
public class FinishPreAuthTradeResponse extends SinapayResponse{
	
}

    