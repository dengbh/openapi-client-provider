package com.acooly.module.openapi.client.provider.sinapay.message.trade;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.annotation.SinaGateWay;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRedirect;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinaDepositStatus;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayGateWayType;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 充值
 *
 * @author xiaohong
 * @create 2018-07-17 18:05
 **/
@Getter
@Setter
@SinaGateWay(SinapayGateWayType.PAYMENT)
@SinapayApiMsg(service = SinapayServiceNameEnum.CREATE_HOSTING_DEPOSIT, type = ApiMessageType.Response)
public class CreateHostingDepositResponse extends SinapayRedirect {
    /**
     * 充值订单号
     */
    @NotEmpty
    @Size(max = 32)
    @ApiItem(value = "out_trade_no")
    private String outTradeNo;

    /**
     * 充值状态
     *
     * 充值状态，详见附录“充值状态”
     */
    @Size(max = 16)
    @ApiItem(value = "deposit_status")
    private String depositStatus;

    /**
     * 后续推进需要的参数
     *
     * 如果支付需要推进则会返回此参数，支付推进时需要带上此参数，ticket有效期为15分钟，可以多次使用（最多5次）
     */
    @Size(max = 100)
    @ApiItem(value = "ticket")
    private String ticket;

    /**
     * 线下支付收款单位
     *
     * 如果支付方式选择线下支付，则会返回
     */
    @Size(max = 200)
    @ApiItem(value = "trans_account_name ")
    private String transAccountName;

    /**
     * 线下支付收款账号
     *
     * 如果支付方式选择线下支付，则会返回
     */
    @Size(max = 100)
    @ApiItem(value = "trans_account_no ")
    private String transAccountNo;

    /**
     * 线下支付收款账号开户行
     *
     * 如果支付方式选择线下支付，则会返回
     */
    @Size(max = 100)
    @ApiItem(value = "trans_bank_brank ")
    private String transBankBrank;

    /**
     * 线下支付收款备注
     *
     * 如果支付方式选择线下支付，则会返回，用于银行备注栏填写
     */
    @Size(max = 100)
    @ApiItem(value = "trans_trade_no ")
    private String transTradeNo;

    /**
     * 收银台重定向地址
     *
     * 当请求参数中的“version”的值是“1.1”时，且支付方式扩展是网银并选择“SINAPAY”跳转新浪收银台时，此参数不为空。
     * 商户系统需要将用户按此参数的值重定向到新浪收银台。其他情况不返回此值，“version”的值是“1.0”时也不返回此值。
     */
    @Size(max = 200)
    @ApiItem(value = "redirect_url ")
    private String redirectUrl;
}
