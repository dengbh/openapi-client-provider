package com.acooly.module.openapi.client.provider.baofup.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPRequest;
import com.acooly.module.openapi.client.provider.baofup.enums.BaoFuPServiceEnum;
import com.acooly.module.openapi.client.provider.baofup.support.BaoFuPAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/4/13 14:56
 */
@Getter
@Setter
@BaoFuPApiMsgInfo(service = BaoFuPServiceEnum.PROTOCOL_CONFIRM_PAY,type = ApiMessageType.Request)
public class BaoFuProtocolConfirmPayRequest extends BaoFuPRequest {

    /**
     * 预支付成功后返回(不用传,分开传入一下字段组建会自动拼装)
     * 格式：预签约唯一码|短信验证码（顺序不能变）
     * 加密方式：Base64转码后，使用数字信封指定的方式和密钥加密
     */
    @Size(max = 255)
    @NotBlank
    @BaoFuPAlias(value = "unique_code",isEncrypt = true)
    private String applyCode;

    /**
     * 预支付唯一码
     */
    @NotBlank
    private String uniqueCode;
    /**
     * 短息验证码
     */
    @NotBlank
    private String smsCode;

    /**
     * 卡信息
     * 当使用信用卡支付时，需上传。
     * 格式：信用卡有效期|安全码(顺序不能变)
     * 加密方式：Base64转码后，使用数字信封指定的方式和密钥加密
     */
    @Size(max = 126)
    @BaoFuPAlias(value = "card_info",isEncrypt = true)
    private String cardInfo;

    /**
     * 组装请求信息
     * @return
     */
    public String getApplyCode() {
        return String.format("%s|%s", getUniqueCode(), getSmsCode());
    }
}
